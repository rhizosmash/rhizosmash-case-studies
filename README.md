rhizoSMASH - Case Studies
=========================

General Survey - Distribution of RhizoSMASH rCGCs
-------------------------------------------------

This study demonstrates the distribution of rhizoSMASH predicted **r**hizosphere competence related **C**atabolic **G**ene **C**lusters (rCGCs) across soil and rhizosphere bacterial genomes. We used the following datasets:

* RefSoil database (["Strategies to improve reference databases for soil microbiomes", J. Choi, et. al.](https://www.nature.com/articles/ismej2016168)) contains over eight hundreds of high-quality soil bacterial genomes, including plant-related bacteria.
* Barley rhizosphere microbiota collection (["Genome-Annotated Bacterial Collection of the Barley Rhizosphere Microbiota", S. Robertson-Albertyn, et. al.](https://journals.asm.org/doi/10.1128/mra.01064-21)) is a small set of 41 bacterial genomes.
* _Arabidopsis_ root microbiota collection (["Functional overlap of the _Arabidopsis_ leaf and root microbiota", Y, Bai, et. al.](https://www.nature.com/articles/nature16192)) contains near two hundreds root-associated bacterial genomes.


Case Study 1 - Predicting Rhizosphere Competence (phenazine-producing _Pseudomonas_ strains)
---------------------------------------------------------------------------------------------

We trained random forests models to show the prediction power of rhizoSMASH rCGCs to rhizosphere competence. In case study 1, a collection of 60 phenazine-producing _Pseudomonas_ strains were used in our analysis (["Metabolic and genomic traits of phytobeneficial phenazine-producing _Pseudomonas_ spp. Are linked to rhizosphere colonization in Arab_idopsis thaliana_ and _Solanum tuberosum_"](https://journals.asm.org/doi/10.1128/AEM.02443-19)).

#### Data Properties:
* High quality bacterial genomes
* All strains are _Pseudomonas_
* Catabolic profiles measured with BioLog phenotyping microarrays
* Rhizosphere competence measured in gnotobiotic environments


Case Study 2 - Predicting Rhizosphere Competence (Mediterranean grassland soil bacteria isolates)
-------------------------------------------------------------------------------------------------

In case study 2, a collection of 26 bacteria strains isolated from soil of Mediterranean grassland dominated by wild barley was used in our analysis (["Dynamic root exudate chemistry and microbial substrate preferences drive patterns in rhizosphere microbial community assembly"](https://www.nature.com/articles/s41564-018-0129-3)).

#### Data Properties:
* Diverse bacterial isolates
* Catabolic profiles measured with exometabolomic footprinting
* Rhizosphere competence measured in microbiota


Extended study - Co-occurence of rhizoSMASH rCGCs and antiSMASH BGCs
--------------------------------------------------------------------

We want to show if rCGCs and BGCs present together frequently in bacterial genomes. This exteded study uses datasets described in "General Survey".
