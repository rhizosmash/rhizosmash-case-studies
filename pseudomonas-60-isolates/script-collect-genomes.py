#!/usr/bin/env python3
"""Download genbank records with given accession"""

from os.path import exists
from sys import stderr
if __name__ == '__main__':
    ## parse command line args
    import argparse
    parser = argparse.ArgumentParser(
        description="Download genbank records with given accession")
    parser.add_argument("-l", "--list",
        type=str, required=True,
        help="Tab-separated file, the last field of each line is"
             "a genbank accession")
    parser.add_argument("-d", "--dir",
        type=str, default="genome-sequences/",
        help="The folder to put downloaded genbank files")

    options = parser.parse_args()
    accession_file = options.list
    output_dir     = options.dir

    if not exists(accession_file):
        print("ERROR: input file not found", file=stderr)
        exit(1)
    if not exists(output_dir):
        print("WARNING: output dir not found, create new dir", file=stderr)
        from os import mkdir
        mkdir(output_dir)

from Bio import Entrez, SeqIO
Entrez.email = "yuze.li@wur.nl"

def download_record_from_ncbi(accession: str, source: str):
    """Run efetch to load genbank record from NCBI nuccore
    Parameters:
        accession (string), NCBI nucleotide accession (e.g. NC_000913 for E. coli K-12)
        source (string), directory where source genbank files are saved
    Returns a SeqRecord class object
    """
    source_path = f"{source}/{accession}.gbk"
    if exists(source_path):
        record = SeqIO.read(source_path, "gb")
    else:
        with Entrez.efetch(db="nuccore", id=accession,
                           rettype="gbwithparts", retmode="text") as f:
            record = SeqIO.read(f, "gb")
        SeqIO.write(record, source_path, "gb")

from tqdm import tqdm

if __name__ == '__main__':
    accessions = []
    with open(accession_file) as f:
        for line in f:
            line = line.strip().split("\t")
            accessions.append(line.pop())
    with tqdm(accessions) as pbar:
        for acc in pbar:
            pbar.set_description(f"Downloading accession: {acc}")
            download_record_from_ncbi(accession=acc, source=output_dir)
