#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo 'Usage: bash run-rhizosmash -l <list> -i <input-dir> -o <output-dir>' >&2
    echo 'Options:' >&2
    echo '    -l list        tab-separated list, last field is the accession' >&2
    echo '    -i input-dir   input dir in which genbank files (accession.gbk)' >&2
    echo '    -o output-dir  output dir for all rhizosmash output' >&2
    exit 1
fi

# detect rhizosmash
if [[ -z $(which rhizosmash) ]] ; then
    echo 'Error: rhizosmash not found in environment' >&2
    exit 1
fi

# parse args
while getopts ":l:i:o:h" opt ; do
    case ${opt} in
        l)
            LIST_FILE=${OPTARG}
            ;;
        i)
            INPUT_DIR=${OPTARG}
            ;;
        o)
            OUTPUT_DIR=${OPTARG}
            ;;
        h|*)
            bash run-rhizosmash.sh
            exit
            ;;
    esac
done

## find io files and run rhizosmash
if [[ ! -f ${LIST_FILE} ]] ; then
    echo "ERROR: input accession list file not found" >&2
    exit
fi
if [[ ! -d ${INPUT_DIR} ]] ; then
    echo "ERROR: input directory not found" >&2
    exit
fi
if [[ ! -d ${OUTPUT_DIR} ]] ; then
    echo "WARNING: output directory not found, try create" >&2
    mkdir ${OUTPUT_DIR}
fi

cat ${LIST_FILE} | awk -v FS='\t' '{print $NF}' | while read acc
do
    input_file=${INPUT_DIR}/${acc}.gbk
    output_dir=${OUTPUT_DIR}/${acc}
    echo "Running rhizosmash on ${acc}"
        if [[ -f ${input_file} ]] ; then
            if [[ -f "${output_dir}/index.html" ]] ; then
                echo "  output exist, skip"
            else
                rhizosmash ${input_file} \
                    --output-dir "${output_dir}" \
                    --genefinding-tool prodigal \
                    --minimal --skip-zip-file \
                    --hmmdetection-strictness strict
            fi
        else
            echo "  input not found, skip"
        fi
done
