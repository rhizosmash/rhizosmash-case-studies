#!/usr/bin/env python3
"""Summarize cluster presence/absense in a table

Usage:
python script-collect-pa.py [-h] [-g GRAND_DIR] [-t TYPE_LIST]

Options:
  -g/--grand-dir GRAND_DIR
        Path to the grand dir containing *smash output dirs
        (default: output/)
  -s/--sub-dirs SUB-DIRS
        Path to a file listing *smash output directories inside grand-dir
        (default: all, using all sub-dirs inside grand-dir)
  -t/--type-list TYPE_LIST
        Path to the type list file (default: .txt)
"""
import argparse

parser = argparse.ArgumentParser(
    description="Summarize *smash cluster presence/absense in a table")
parser.add_argument("-g", "--grand-dir",
    type=str, default="output/",
    help="Path to the grand dir containing *smash output dirs "
         "(default: output/)")
parser.add_argument("-s", "--sub-dirs",
    type=str, default="all",
    help="Path to a file listing *smash output directories inside grand-dir"
         "(default: all, using all sub-dirs inside grand-dir)")
parser.add_argument("-t", "--type-list",
    type=str, required=True,
    help="Path to the type list file")
args = parser.parse_args()

grand_dir = args.grand_dir
sub_dir_list = args.sub_dirs
type_list = args.type_list

from Bio import SeqIO
from os import listdir, path

if sub_dir_list == "all":
    sub_dirs = [s for s in listdir(grand_dir)
        if path.isdir(path.join(grand_dir, s))]
else:
    with open(dir_list, "r") as f:
        sub_dirs = [line.strip().split()[0] for line in f]

with open(type_list, "r") as f:
    clusters = [line.strip().split("\t")[1] for line in f]

output = {output_dir : {cluster : False
                    for cluster in clusters}
          for output_dir in sub_dirs}

for output_dir in sub_dirs:
    for fn in listdir(path.join(grand_dir, output_dir)):
        if "region" in fn and fn.endswith("gbk"):
            record = SeqIO.read(
                path.join(grand_dir, output_dir, fn), "genbank")
            for feature in record.features:
                if feature.type == "region":
                    for cluster in feature.qualifiers["product"]:
                        output[output_dir][cluster] = True

print("GenomeAcc\t" + "\t".join(clusters))
for acc in sub_dirs:
    print(acc + "\t" + "\t".join([
        str(output[acc][cluster]).upper()
        for cluster in clusters]))
