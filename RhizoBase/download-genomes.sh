#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo 'Usage: bash download-genomes.sh all | [label] <label> ...' >&2
    echo 'Arguments:' >&2
    echo '    all    download all collections.' >&2
    echo '    label  collection label, should be one of the following' >&2
    echo '           REFSOIL  (RefSoil soil bacteria collection)' >&2
    echo '           RHIZATHA (Arabidopsis rhizobacteria collection)' >&2
    echo '           SOILATHA (Arabidopsis soil bacteria collection)'
    echo '           RHIZTAES (Wheat rhizobacteria collection)' >&2
    echo '           RHIZHVUL (Barley rhizobacteria collection)' >&2
    echo '           RHIZOSAT (Rice rhizobacteria collection)' >&2
    echo '           RHIZSLYC (Tomato rhizobacteria collection)' >&2
    exit 1
fi

# parse args
validcollections="^(REFSOIL|SOIL(ATHA)|RHIZ(ATHA|TAES|HVUL|OSAT|SLYC))$"
if [[ $1 == "all" ]] ; then
    collections="REFSOIL RHIZATHA SOILATHA RHIZTAES RHIZHVUL RHIZOSAT RHIZSLYC"
else
    collections=${@}
fi

# checksum, linux bash uses md5sum but mac terminal uses md5
if [[ -f $(which md5sum) ]] ; then
    checksum() {
        testsum=$(md5sum ${1})
        testsum=${testsum::32}
        echo ${testsum}
    }
else
    checksum() {
        testsum=$(md5 -q ${1})
        echo ${testsum}
    }
fi
echo -n > checksum-failed.log

# download collections
for collection in ${collections} ; do
    if [[ ! ${collection} =~ ${validcollections} ]] ; then
        echo Invalid genome collection: ${collection} >&2
    else
        totalnum=$(cat collections/${collection}.list.txt | wc -l)
        totalnum=$((totalnum+0))
        currnum=1
        cat collections/${collection}.list.txt \
        | while IFS=$'\t' read rhizoid taxid acc complete path ; do
            echo -ne "\rDownloading collection ${collection} | "
            echo -ne "${acc} | ${currnum} out of ${totalnum} | "
            printf "%2d %%" $(((currnum-1)*100/totalnum))

            # get md5
            url=${path}/md5checksums.txt
            base_name=${path##*/}
            while read refsum filename ; do
                if [[ ${filename} == *${base_name}_genomic.gbff.gz ]] ; then
                    refsum_gb=${refsum}
                elif [[ ${filename} == *${base_name}_genomic.fna.gz ]] ; then
                    refsum_fa=${refsum}
                fi
            done < <(wget -qO - ${url})

            # get genbank
            url=${path}/${base_name}_genomic.gbff.gz
            if [[ ! -f assemblies/${acc}.gb.gz ]] ; then
                wget -qO assemblies/${acc}.gb.gz ${url}
            fi
            # check md5 sum
            testsum=$(checksum assemblies/${acc}.gb.gz)
            if [ ${refsum_gb} != ${testsum} ] ; then
                echo
                for testnum in 5 4 3 2 1 ; do
                    echo "${acc} genbank download does not match md5, try #${testnum}"
                    wget -qO assemblies/${acc}.gb.gz ${url}
                    testsum=$(checksum assemblies/${acc}.gb.gz)
                    if [ ${refsum_gb} == ${testsum} ]; then break ; fi
                done
            fi
            if [ ${refsum_gb} != ${testsum} ] ; then
                echo "${rhizoid} genbank download failed to match md5sum"
                echo ${rhizoid} ${acc} genbank > checksum-failed.log
                rm assemblies/${acc}.gb.gz
            fi

            # get fasta dna
            url=${path}/${base_name}_genomic.fna.gz
            if [[ ! -f assemblies/${acc}.fasta.gz ]] ; then
                wget -qO assemblies/${acc}.fasta.gz ${url}
            fi
            # check md5 sum
            testsum=$(checksum assemblies/${acc}.fasta.gz)
            if [ ${refsum_fa} != ${testsum} ] ; then
                echo
                for testnum in 5 4 3 2 1 ; do
                    echo "${acc} fasta download does not match md5, try #${testnum}"
                    wget -qO assemblies/${acc}.fasta.gz ${url}
                    testsum=$(checksum assemblies/${acc}.fasta.gz)
                    if [ ${refsum_fa} == ${testsum} ]; then break ; fi
                done
            fi
            if [ ${refsum_fa} != ${testsum} ] ; then
                echo "${rhizoid} fasta download failed to match md5sum"
                echo ${rhizoid} ${acc} fasta > checksum-failed.log
                rm assemblies/${acc}.fasta.gz
            fi

            currnum=$((currnum+1))
        done
        echo -e "\b\b\b\bFinished"
    fi
done