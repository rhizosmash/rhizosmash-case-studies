#!/user/bin/env bash

# settings
REFSOIL_PATH="RefSoil_v1.txt"
if [[ ! -f ${REFSOIL_PATH} ]] ; then
    echo "[Error] RefSoil Record not found" >&2
    echo "Please fownload RefSoil record from <github.com/germs-lab/ref_soil>" >&2
    exit 1
fi
apibase='https://eutils.ncbi.nlm.nih.gov/entrez/eutils'

# gethering information
INCID=0
cat ${REFSOIL_PATH} | tail +2 | awk -F "\t" \
'# read 10 accessions at a time
BEGIN { counter = 0 }
$7 == "Bacteria" {
    if (counter == 0) printf("(%s", $1) ; else printf(" OR %s", $1)
    counter = counter + 1;
    if (counter == 10) { counter = 0 ; printf(") AND latest\n") }
}
END { printf(") AND latest\n") }' \
| while read query ; do
    echo query: $query >&2
    # esearch
    url="${apibase}/esearch.fcgi"
    url="${url}?db=assembly&usehistory=y&term=${query// /%20}"
    read querykey webenv <<< $(
        wget -qO- ${url} \
        | xtract -pattern eSearchResult -element QueryKey,WebEnv
    )
    sleep 0.4
    # esummary
    url="${apibase}/esummary.fcgi"
    url="${url}?db=assembly&query_key=${querykey}&WebEnv=${webenv}"
    while IFS=$'\t' read taxid acc status ftppath ; do
        printf "REFSOIL%05d\t%s\t%s\t%s\t%s\n" \
               ${INCID} ${taxid} ${acc} "${status}" ${ftppath}
        INCID=$((INCID+1))
    done < <(
        wget -qO- ${url} \
        | xtract -pattern DocumentSummary \
                 -element Taxid,Genbank,AssemblyStatus,FtpPath_GenBank
    )
    sleep 0.4
done