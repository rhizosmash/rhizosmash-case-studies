#!/user/bin/env bash

# settings
BIOPROJECT_ACC="PRJNA297942"

# gethering information
INCID=0
esearch -db assembly -query "${BIOPROJECT_ACC}[BioProject] AND latest" \
| esummary \
| xtract -pattern DocumentSummary \
         -element Taxid,Genbank,AssemblyStatus,FtpPath_GenBank \
| while IFS=$'\t' read taxid acc status ftppath ; do
    printf "RHIZATHA%05d\t%s\t%s\t%s\t%s\n" \
           ${INCID} ${taxid} ${acc} "${status}" ${ftppath}
    INCID=$((INCID+1))
done