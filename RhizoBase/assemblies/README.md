# RhizoRef genome database collections

Collection labels:
- **REFSOIL**: full genome assemblies of bacterial genomes refered from the *RefSoil* database
- **RHZATHA**: *__A. thaliana__* rhizobacteria genome collection, refered from the *atSPHERE* database
- **RHZHVUL**: **barley** rhizobacteria genome collection
- **RHZTARS**: **wheat** rhizobacteria genome collection
- **RHZOSAT**: **rice** rhizobacteria genome collection
- **RHZSLYC**: **tomato** rhizobacteria genome collection

Genome assemblies are downloaded from Genbank.