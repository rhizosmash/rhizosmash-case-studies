#!/usr/bin/env python3
"""Generate a genome accession list with sufficient number of
genomes per taxa label.

Make sure 'metadata-rhizobase-taxa.txt' is in your directory.

Usage:
python <script> [-h] -t TAXA -s THRESHOLD -c COMPLETENESS -l LIST
"""

import argparse

## parse command line args
parser = argparse.ArgumentParser(
    description="""
Generate a genome accession list with sufficient number of
genomes per taxa label.
Make sure 'metadata-rhizobase-taxa.txt' is in your directory.""")
parser.add_argument("-t", "--taxa",
    type=str, required=True,
    help="Taxonomy annotation file output from script-collect"
         "-rhizobase-taxa.py")
parser.add_argument("-s", "--threshold",
    type=int, default=8,
    help="Minimum number of genomes as sufficient "
         "(default: 8)")
parser.add_argument("-c", "--completeness",
    type=int, default=1,
    help="Completeness level, 1~4 for complete, chromosome, "
         "scaffold, contig")
parser.add_argument("-l", "--list",
    type=str, required=True,
    help="File contains a list of genome accession to limit. "
         "use 'all' if no limit.")
options = parser.parse_args()
threshold = options.threshold
list_file = options.list
comp_code = options.completeness
input_file = options.taxa
comp_levels = {"Complete Genome" : 1, "Chromosome" : 2,
               "Scaffold" : 3,        "Contig" : 4}

## parse limit list if asked
if list_file != "all":
    limit = True
    acc_include = set()
    with open(list_file, "r") as f:
        for line in f:
            acc_include.add(line.strip())
else:
    limit = False

## parse metadata file and count
counts = dict()
with open(input_file, "r") as f:
    for line in f:
        genome_acc, _, taxa, _, completeness =\
            line.strip().split("\t")
        if (not limit or genome_acc in acc_include) and \
        comp_levels[completeness] <= comp_code:
            if taxa in counts:
                counts[taxa].append(genome_acc)
            else:
                counts[taxa] = [genome_acc]

## apply threshold and output list
for taxa in counts:
    if len(counts[taxa]) >= threshold:
        for acc in counts[taxa]:
            print(f"{acc}\t{taxa}")
