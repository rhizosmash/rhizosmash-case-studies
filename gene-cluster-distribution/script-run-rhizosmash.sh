#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo 'Usage: bash run-rhizosmash [-n np] (all | [label] <label> ...)' >&2
    echo 'Options:' >&2
    echo '    -n np  number of parallel processes for rhizosmash (default 1)' >&2
    echo '    -c nc  number of threads for hmmsearch/blast (default 4)' >&2
    echo 'Arguments:' >&2
    echo '    all    run rhizosmash for all collections.' >&2
    echo '    label  collection label, should be one of the following' >&2
    echo '           REFSOIL  (RefSoil soil bacteria collection)' >&2
    echo '           RHIZATHA (Arabidopsis rhizobacteria collection)' >&2
    echo '           SOILATHA (Arabidopsis soil bacteria collection)'
    echo '           RHIZTAES (Wheat rhizobacteria collection)' >&2
    echo '           RHIZHVUL (Barley rhizobacteria collection)' >&2
    echo '           RHIZOSAT (Rice rhizobacteria collection)' >&2
    echo '           RHIZSLYC (Tomato rhizobacteria collection)' >&2
    exit 1
fi

# detect rhizosmash
if [[ -z $(which rhizosmash) ]] ; then
    echo 'Error: rhizosmash not found in environment' >&2
    exit 1
fi

# parse args
np=1
nc=4
while getopts ":n:c:h" opt ; do
    case ${opt} in
        n)
            np=${OPTARG}
            ;;
        c)
            nc=${OPTARG}
            ;;
        h|*)  bash run-rhizosmash.sh
            exit
            ;;
    esac
done
shift $(( OPTIND-1 ))

validcollections="^(REFSOIL|SOIL(ATHA)|RHIZ(ATHA|TAES|HVUL|OSAT|SLYC))$"
if [[ $1 == "all" ]] ; then
    collections="REFSOIL RHIZATHA SOILATHA RHIZTAES RHIZHVUL RHIZOSAT RHIZSLYC"
else
    collections=${@}
fi

## set parallel workers with fifo
mkfifo .PIPE-$$
exec 37<>.PIPE-$$
rm .PIPE-$$
for (( i=${np} ; i>0 ; i-- )) ; do echo 000 >&37 ; done

## find io files and run rhizosmash
for collection in ${collections} ; do
    if [[ ! ${collection} =~ ${validcollections} ]] ; then
        echo 'Warning: not valid genome collection: ${collection}' >&2
    else
        totalnum=$(cat ../RhizoBase/collections/${collection}.list.txt | wc -l)
        totalnum=$((totalnum+0))
        currnum=1
        cat ../RhizoBase/collections/${collection}.list.txt \
        | cut -f1,3 \
        | while read rhizoid acc ; do
            assembly_path="../RhizoBase/assemblies/${acc}.gb.gz"
            echo -ne "\rRunning rhizosmash on ${collection} | "
            echo -ne "${rhizoid} | ${currnum} out of ${totalnum} | "
            printf "%2d %%" $(((currnum-1)*100/totalnum))
            if [[ -f ${assembly_path} ]] ; then
                if [[ -f "rhizosmash-output/${acc}/index.html" ]] ; then
                    echo -n " |        output exist, skip |"
                else
                    if [[ -d "rhizosmash-output/${acc}" ]] ; then
                        echo -n " | output incomplete, re-run |"
                        rm -r "rhizosmash-output/${acc}"
                    else
                        echo -n " |            run rhizosmash |"
                    fi
                    ## queue the fifo
                    read flag <&37
                    (
                        rhizosmash ${assembly_path} \
                            --output-dir "rhizosmash-output/${acc}" \
                            --genefinding-tool prodigal \
                            --minimal --skip-zip-file \
                            --hmmdetection-strictness relaxed \
                            -c ${nc}
                        echo 000 >&37
                    )&
                fi
            else
                echo  "|           input not found |"
            fi
            currnum=$((currnum+1))
        done
        echo -e "\b\b\b\bFinished"
    fi
done