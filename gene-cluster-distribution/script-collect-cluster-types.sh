#!/usr/bin/env bash

# help doc
if [[ $# -eq 0 ]] ; then
    echo 'Print cluster rule names (types) to stdout'
    echo 'Usage: bash {script} -p path [-s strictness]' >&2
    echo 'Options:' >&2
    echo '    -p path to antismash lib dir' >&2
    echo '    -s [strict|relaxed|loose] (default: strict)' >&2
    echo '    -h print help doc' >&2
    exit 1
fi

# parse args
location=~/Desktop/project/rhizosmash/antismash/detection/hmm_detection/cluster_rules/
strictness=strict
validstrictness="^(strict|relaxed|loose)$"
while getopts ":p:s:h" opt ; do
    case ${opt} in
        p)
            location=${OPTARG}
            ;;
        s)
            if [[ ! ${strictness} =~ ${validstrictness} ]] ; then
                bash $0
                exit
            fi
            strictness=${OPTARG}
            ;;
        h|*)  bash $0
            exit
            ;;
    esac
done
shift $(( OPTIND-1 ))

# catch file
parse_rules () {
    paste \
    <(cat ${1} | grep "^    CATEGORY" | tr -s " " | cut -f3 -d" ") \
    <(cat ${1} | grep "^RULE" | tr -s " " | cut -f2 -d" ") | \
    while read category name ; do
        echo -e "${category}\t${name}\t${2}"
    done
}

case ${strictness} in
    strict )
        parse_rules ${location}/strict.txt strict
        ;;
    relaxed )
        parse_rules ${location}/strict.txt strict
        parse_rules ${location}/relaxed.txt relaxed
        ;;
    loose )
        parse_rules ${location}/strict.txt strict
        parse_rules ${location}/relaxed.txt relaxed
        parse_rules ${location}/loose.txt loose
        ;;
    * )
        echo "Invalid input"
        ;;
esac
