#!/usr/bin/env python3
"""Summarize cluster presence/absense in a table

Usage:
python script-collect-pa.py [-h] [-g GRAND_DIR] [-t TYPE_LIST]

Options:
  -g/--grand-dir GRAND_DIR
        Path to the grand dir containing *smash output dirs
        (default: output/)
  -s/--sub-dirs SUB-DIRS
        Path to a file listing *smash output directories inside grand-dir
        (default: all, using all sub-dirs inside grand-dir)
  -t/--type-list TYPE_LIST
        Path to the type list file (default: .txt)
  -l/--level {genome,chromosome}
        Sequence level to summarize (default: genome)
  -p/--presence {ps/count}
        Type of summary, p/a or count (default: pa)
"""
import argparse

parser = argparse.ArgumentParser(
    description="Summarize *smash cluster presence/absense in a table")
parser.add_argument("-g", "--grand-dir",
    type=str, default="output/",
    help="Path to the grand dir containing *smash output dirs "
         "(default: output/)")
parser.add_argument("-s", "--sub-dirs",
    type=str, default="all",
    help="Path to a file listing *smash output directories inside grand-dir"
         "(default: all, using all sub-dirs inside grand-dir)")
parser.add_argument("-t", "--type-list",
    type=str, required=True,
    help="Path to the type list file")
parser.add_argument("-l", "--level",
    type=str, choices=["genome", "chromosome"], default="genome",
    help="Sequence level to summarize "
         "(default: genome)")
parser.add_argument("-p", "--presence",
    type=str, choices=["pa", "count"], default="pa",
    help="Type of summary, p/a or count (default: pa)")
args = parser.parse_args()

grand_dir = args.grand_dir
sub_dir_list = args.sub_dirs
type_list = args.type_list
seq_level = args.level
summary = args.presence

from Bio import SeqIO
from os import listdir, path

if sub_dir_list == "all":
    sub_dirs = [s for s in listdir(grand_dir)
        if path.isdir(path.join(grand_dir, s))]
else:
    with open(sub_dir_list, "r") as f:
        sub_dirs = [line.strip().split()[0] for line in f]

with open(type_list, "r") as f:
    clusters = [line.strip() for line in f]

output = dict()

for output_dir in sub_dirs:
    index = output_dir
    for fn in listdir(path.join(grand_dir, output_dir)):
        if "region" in fn and fn.endswith("gbk"):
            record = SeqIO.read(
                path.join(grand_dir, output_dir, fn), "genbank")
            if seq_level == "chromosome":
                index = record.name
            if index not in output:
                if summary == "pa":
                    output[index] = {cluster : False for cluster in clusters}
                elif summary == "count":
                    output[index] = {cluster : 0 for cluster in clusters}
            for feature in record.features:
                if feature.type == "region":
                    for cluster in feature.qualifiers["product"]:
                        if cluster in output[index]:
                            if summary == "pa":
                                output[index][cluster] = True
                            elif summary == "count":
                                output[index][cluster] += 1

if seq_level == "genome":
    print("GenomeAcc\t" + "\t".join(clusters))
else:
    print("ChromosomeAcc\t" + "\t".join(clusters))

for acc in output:
    print(acc + "\t" + "\t".join([
        str(output[acc][cluster]).upper()
        for cluster in clusters]))
