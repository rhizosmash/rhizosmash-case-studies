#!/usr/bin/env python3
"""Report Representative Genomes Given Mash Distance List

This script take a mash dist output as input and print a table of 
unique (at a given distance threshold) genome accessions
"""

import argparse
from collections import deque

## parse command line args
parser = argparse.ArgumentParser(
    description="""
This script take a mash dist output as input and print a table of 
unique (at a given similarity threshold) genome accessions.""")
parser.add_argument("-i", "--input",
    type=str, required=True,
    help="mash dist list file")
parser.add_argument("-t", "--threshold",
    type=float, default=0.01,
    help="the distance threshold "
         "(default: 0.01)")
options = parser.parse_args()
file_path = options.input
threshold = options.threshold

## build graph
genomes = set()
edges = dict()
with open(file_path) as f:
    for line in f:
        ref, qry, distance, _, _ =\
            line.strip().split("\t")
        ref = ref.split("/")[-1][:-9]
        qry = qry.split("/")[-1][:-9]
        distance = float(distance)
        if distance <= threshold:
            genomes.update([ref, qry])
            if ref in edges:
                edges[ref].add(qry)
            else:
                edges[ref] = set([qry])
            if qry in edges:
                edges[qry].add(ref)
            else:
                edges[qry] = set([ref])

## load genome completeness
rhizobase_path = "../RhizoBase/collections"
collections = ["REFSOIL", "RHIZATHA", "SOILATHA",
               "RHIZTAES", "RHIZHVUL", "RHIZOSAT", "RHIZSLYC"]
completeness = dict()
for collection in collections:
    fn = f"{rhizobase_path}/{collection}.list.txt"
    with open(fn, "r") as f:
        for line in f:
            _, _, acc, comp, _ = line.strip().split("\t")
            completeness[acc] = comp
comp_levels = ["Complete Genome", "Chromosome", "Scaffold", "Contig"]

## BFS graph for cc
while genomes:
    g = genomes.pop()
    trv = deque([g])
    log = []
    while trv:
        cur = trv.popleft()
        log.append(cur)
        for nex in edges[cur]:
            if nex in genomes:
                trv.append(nex)
                genomes.remove(nex)
    if len(log) == 1:
        print(log[0])
    else:
        comp = {l : [] for l in comp_levels}
        for acc in log:
            [completeness[acc]].append(acc)
        for l in comp_levels:
            if len(comp[l]) > 0:
                print(comp[l][0])
