#!/usr/bin/env python3
"""Summarize RhizoBase taxonomy information

This script will read RhizoBase collection lists and summarized taxa info
at a given clade level. The script will filter for clades having a given
threadhold number of genomes.

Usage:
python <script> [-h] -t TAXA_LEVEL
"""

import argparse

## parse command line args
parser = argparse.ArgumentParser(
    description="""
This script will read RhizoBase collection lists and summarized taxa info
at a given clade level.""")
parser.add_argument("-t", "--taxa-level",
    type=str, default="genus",
    help="The clade level to summarize RhizoBase genomes "
         "(default: genus)")
# parser.add_argument("-n", "--threshold",
#     type=int, default=8,
#     help="Number of genomes to keep a clade in the summary "
#          "(default: 8)")
# parser.add_argument("-l", "--list",
#     type=str, required=False,
#     help="File contains a list of genome accessions to include")
options = parser.parse_args()
taxa_level = options.taxa_level
# threshold = options.threshold
# list_file = options.list

## initialize ncbi taxa finder
from ete3 import NCBITaxa
ncbitaxa = NCBITaxa()

## envrionment settings
rhizobase_path = "../RhizoBase/collections"
collections = ["REFSOIL", "RHIZATHA", "SOILATHA",
               "RHIZTAES", "RHIZHVUL", "RHIZOSAT", "RHIZSLYC"]

## parse list file if asked
# if list_file is not None:
#     acc_included = set()
#     with open(list_file, "r") as f:
#         for line in f:
#             acc_included.add(f.strip())

## find lineage
results = dict()
# counts = dict()
for collection in collections:
    fn = f"{rhizobase_path}/{collection}.list.txt"
    with open(fn, "r") as f:
        for line in f:
            _, taxa_id, genome_acc, completeness, _ = line.strip().split("\t")
            if genome_acc not in results:
                lineage = ncbitaxa.get_lineage(taxa_id)
                ranks = ncbitaxa.get_rank(lineage)
                names = ncbitaxa.get_taxid_translator(lineage)
                for tid in lineage:
                    if ranks[tid] == taxa_level:
                        results[genome_acc] = (
                            tid, names[tid], [collection], completeness)
                        # if tid in counts:
                        #     counts[tid] += 1
                        # else:
                        #     counts[tid] = 1
                        break
            elif collection not in results[genome_acc][2]:
                results[genome_acc][2].append(collection)

## apply threshold
# used_clades = {tid for tid in counts if counts[tid] >= threshold}
print("GenomeAcc\tTaxaID\tName\tCollection\tCompleteness")
for genome_acc in results:
    taxa_id, clade_name, collection, completeness = results[genome_acc]
    # if taxa_id in used_clades:
    collection = ",".join(collection)
    print(f"{genome_acc}\t{taxa_id}\t{clade_name}"
          f"\t{collection}\t{completeness}")
