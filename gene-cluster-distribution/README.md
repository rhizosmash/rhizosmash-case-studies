## Introduction

This section contains the scripts for visualizing and analysing the
distribution of rhizoSMASH predicted-rCGC across bacterial genomes.
We have three sub-topics under this section:

1.  rCGC distribution in bacterial genomes
2.  functional differentiation of _Burkholderia_ replicons
3.  functional differentiation between soil and root isolates

## Prepare Data

### Prerequisites

1.  installation of __rhizoSMASH__ and __mash__
2.  genome sequence collection __RhizoBase__ downloaded

### Get RhizoBase and rhizoSMASH metadata

We need metadata about the RhizoBase genomes and rhizoSMASH rCGCs
to analyze and visualize our data. The RhizoBase metadata was also
used to make genome lists for different purpose.

The rhizoSMASH metadata is simply a table of rCGC category (e.g. 
Carbohydrate, Amino-acid, etc.), type name (e.g. Xylose, L-Proline,
etc.), and rule strictness. Use this script to get the list:

```bash
bash script-collect-cluster-types.sh -p $RULES -s relaxed > metadata-cluster-types.txt
```

where `$RULES` is the directory stores the rule definition files in
rhizoSMASH. Say if your rhizoSMASH is downloaded at `${RS}`,
the rules is located at
`${RS}/antismash/detection/hmm_detection/cluster_rules/`.

The RhizoBase metadata contains taxonomy, assembly level, and
RhizoBase collection labels for each genome. It can be generated
with the following script:

```bash
python script-collect-rhizobase-taxa.py -t genus > metadata-rhizobase-taxa.txt
```

Our following analysis and visualization will use genus level
taxonomy information.

### Run rhizoSMASH on RhizoBase genomes

Please make sure the directory `rhizosmash-output/` exists under this
directory. We recommend running this part on a computational server, 
because this could take many hours to finish. This script enables to 
execute multiple __rhizoSMASH__ runs in parallel using FIFO control,
and __rhizoSMASH__ itself calls __hmmer__ with multithreading. Please
be careful not to crowd your server.

``` bash
bash script-run-rhizosmash.sh -n $NP -c $NC all
```

where `$NP` is the number of parallel __rhizoSMASH__ runs, and `$NC`
is the number of thread for __hmmer__ in each rhizoSMASH session.

### Deduplicate RhizoBase genomes

Although we ran __rhizoSMASH__ on the full RhizoBase collection, we
had to remove duplicated genomes for further analysis (e.g.
__Big-SCAPE__ for rule tuning and distribution visualization in this
section).

We used __mash__ to find similar genomes. We used the k-mer size 32 
for __mash sketch__ to make better resolution. We kept genome pairs
with distance index less than 0.1 in the output from __mash dist__ 
to reduce file size.

```bash
mash sketch -o mash-sketch-rhizobase.msh -l <(ls ../RhizoBase/assemblies/*.fasta.gz) -S 37 -k 32
mash dist -d 0.1 mash-sketch-rhizobase.msh mash-sketch-rhizobase.msh > mash-dist-rhizobase.txt
```

The R-markdown document `visualize-rhizobase-genome-distance.Rmd` 
contains a short analysis on the distribution the __mash dist__ 
distance indexes between RhizoBase genome pairs. Accroding to the
density plot of pairwise distances, we dicide to use 0.005 as the


Using script `script-get-unique-genome-list.py`, we can make a list 
of unique genomes based on the __mash dist__ output. The script first
generate a graph using a threshold on the distance indexes, then runs
a BFS on the network and output the first element scanned in each 
connected component.

```bash
python script-get-unique-genome-list.py - > list-dedup.txt
```

### Extract rCGC presence/Absence table for dedup RhizoBase genomes

### Summarize taxonomy metadata for dedup RhizoBase genomes

### Make _Burkholderia_ genome list

For the analysis on _Burkholderia_ chromids, we need to make a list
of _Burkholderia_ genomes based on RhizoBase.

### Data preparation summary

At the end of the data preparation phase, we will have the following 
(meta)data and list files for analysis and visualization:

* `data-rhizobase-presence-absence.txt`
  rCGC presence-absence data for all RhizoBase entries.
* `metadata-cluster-types.txt`
  list of cluster type names defined in rhizoSMASH.
* `metadata-rhizobase.txt`
  metadata table of RhizoBase, contains taxa, completeness and
  RhizoBase collection info.
* `list-dedup.txt`
  genome accession list of deduplicated RhizoBase collection, used
  for following Big-SCAPE analysis.
* `list-dedup-complete-sufficient.txt`
  genome accession list of completely sequenced genomes in the
  deduplicated RhizoBase collection.
* `list-burkholderia-complete.txt`
  genome accession list of completely sequenced _Burkholderia_
  genomes in RhizoBase.


## Analysis and Visualization
